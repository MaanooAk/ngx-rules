# @universis/ngx-rules
Universis client library for managing rules and requirements for any educational subject.

## Install
    npm i @universis/ngx-rules

## Configure 

Modify angular.json and include the following scripts and styles:

    "styles": [
      ...
      "node_modules/gijgo/css/gijgo.min.css",
      ...
    ],
    "scripts": [
      "node_modules/jquery/dist/jquery.js",
      "node_modules/@universis/ngx-rules/assets/gijgo/core.js",
      "node_modules/@universis/ngx-rules/assets/gijgo/draggable.js",
      "node_modules/@universis/ngx-rules/assets/gijgo/droppable.js",
      "node_modules/@universis/ngx-rules/assets/gijgo/tree.js",
      ...
    ]

## Development

Add `ngx-rules` repository as submodule of an angular cli application:

    git submodule add https://gitlab.com/universis/ngx-rules.git projects/ngx-rules

Note: Submodule root e.g. `projects/ngx-rules` should be under `newProjectRoot` dir defined in angular.json

Modify tsconfig.app.json and include `@universis/ngx-rules` in paths:

    {
      ...
      "compilerOptions": {
          "paths": {
              ...,
              "@universis/ngx-rules": [
                  "projects/ngx-rules/src/public_api"
              ]
          }
      }
    }

## Build

If you want to build `@universis/ngx-rules` package modify `angular.json` and add `ngx-rules` node under `projects`:

    {
      "$schema": "./node_modules/@angular/cli/lib/config/schema.json",
      "version": 1,
      "newProjectRoot": "packages",
      "projects": {
          ...,
          
          "ngx-rules": {
            "root": "projects/ngx-rules",
            "sourceRoot": "projects/ngx-rules/src",
            "projectType": "library",
            "prefix": "lib",
            "architect": {
              "build": {
                "builder": "@angular-devkit/build-ng-packagr:build",
                "options": {
                  "tsConfig": "projects/ngx-rules/tsconfig.lib.json",
                  "project": "projects/ngx-rules/ng-package.json"
                }
              }
            }
          }          
      }
