
import {en} from './rules.en';
import {el} from './rules.el';

export const locales = {
  en,
  el
}
